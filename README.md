# msolve
![Travis CI](https://api.travis-ci.org/mkb2091/msolve.svg?branch=master)

A WIP sudoku solving library

# Goals

Add backtracker

Add SIMD support to improve speed

Potentially add GPU support to maximise speed
